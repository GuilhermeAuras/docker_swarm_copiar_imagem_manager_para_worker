#!/bin/bash

clear

# Definir o endereço do servidor docker02
DOCKER02="root@IP_VM_WORKER"

# Listar imagens Docker no docker01
echo "Listando imagens Docker disponíveis:"
docker images

# Pedir ao usuário para inserir o nome da imagem a ser transferida
echo "Digite o nome da imagem que você deseja transferir (no formato nome:tag):"
read image_name

# Verificar se a imagem existe
if [[ "$(docker images -q $image_name 2> /dev/null)" == "" ]]; then
  echo "Imagem não encontrada: $image_name"
  exit 1
fi

# Salvar a imagem em um arquivo tar
image_file=$(echo $image_name | tr ":" "_").tar
docker save $image_name > $image_file

# Transferir a imagem para o docker02
echo "Transferindo a imagem para o docker02..."
scp $image_file $DOCKER02:/tmp/

# Carregar a imagem no docker02
echo "Carregando a imagem no docker02..."
ssh $DOCKER02 "docker load < /tmp/$image_file"

# Remover o arquivo tar no docker02 e no docker01
echo "Limpando..."
ssh $DOCKER02 "rm /tmp/$image_file"
rm $image_file

echo "Processo concluído."